using System;
using System.Collections;

namespace QueueFIFODataStructure
{
    class Program
    {
        // Driver code 
        public static void Main()
        {
            // Creating a Queue  
            Queue myQueue = new Queue();

            // Print the number of items in the queue
            Console.WriteLine("Number of items in the queue: " + myQueue.Count);

            // Add elements to the queue
            myQueue.Enqueue("Item One");
            myQueue.Enqueue("Item Two");
            myQueue.Enqueue("Item three");

            // Print the number of items in the queue
            Console.WriteLine("Number of items in the queue after enqueue: " + myQueue.Count);

            // Remove items from the queue
            myQueue.Dequeue();

            // Print the number of items in the queue after the dequeue function
            Console.WriteLine("Number of items in the queue after dequeue: " + myQueue.Count);
        }
    }
}