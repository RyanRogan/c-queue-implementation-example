# README #

This program is a basic example of how to implement a queue, add items to to the queue, and remove items from the queue.

The enqueue function is used to add items to the queue.
The dequeue function is used to remove items from the queue.